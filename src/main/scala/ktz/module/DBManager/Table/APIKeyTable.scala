package ktz.module.DBManager.Table

import slick.driver.H2Driver.api._
import slick.lifted.ProvenShape
import scala.concurrent.ExecutionContext.Implicits.global
/**
 * Created by ktz on 15. 6. 2.
 */

class APIKeyTable(tag : Tag) extends Table[(String, String)](tag, "APIKeys") {
  def APIKey = column[String]("APIKey", O.PrimaryKey)
  def KeyValue = column[String]("Value")

  override def * : ProvenShape[(String, String)] = (APIKey, KeyValue)
}