package ktz.module.DBManager

import ktz.module.DBManager.Table.APIKeyTable

import scala.concurrent.Future
import slick.backend.DatabasePublisher
import slick.driver.H2Driver.api._
import slick.jdbc.meta.MTable

/**
 * Created by ktz on 15. 6. 13.
 */
trait H2Database extends DBManager[APIKeyTable, Database]{
  def IsExistRow(key : Any, db: Database): Future[Boolean] = {
    val query = TableQuery[APIKeyTable]
    db.run(query.filter(_.APIKey === key.asInstanceOf[String]).exists.result)
  }

  private def GetH2Database : Database = Database.forConfig("h2mem1")
}
