package ktz.module.DBManager

import java.sql.SQLDataException

/**
 * Created by ktz on 15. 6. 13.
 */
class RowExistException(msg : String) extends SQLDataException(msg)
class NoRowExistException(msg : String) extends SQLDataException(msg)
