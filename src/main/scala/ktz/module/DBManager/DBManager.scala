package ktz.module.DBManager

import java.sql.SQLDataException

import ktz.module.DBManager.NoRowExistException

import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by ktz on 15. 6. 13.
 */
trait DBManager[RowType, DBType]{
  def AddRow(rowToAdd : RowType)(db : DBType)(AddFunction : => Boolean) : Future[Boolean] =
    DoIfExistOrNot(rowToAdd, new RowExistException("Row already exist"), false)(db)(AddFunction)

  def DeleteRow(rowToDelete : RowType)(db : DBType)(DeleteFunction : => Boolean) : Future[Boolean] =
    DoIfExistOrNot(rowToDelete, new NoRowExistException("No such a row"), true)(db)(DeleteFunction)

  def UpdateRow(rowToUpdate : RowType)(db : DBType)(UpdateFunction : => Boolean) : Future[Boolean] =
    DoIfExistOrNot(rowToUpdate, new NoRowExistException("No such a row"), true)(db)(UpdateFunction)

  def DoIfExistOrNot(rowToOperate : RowType, sqlDataException: SQLDataException, conditionExist : Boolean)(db : DBType)(Operation : => Boolean) : Future[Boolean] = Future[Boolean]{
    if(Await.result(IsExistRow(rowToOperate, db), 10 second) == conditionExist)
      Operation
    else
      throw sqlDataException
  }

  def IsExistRow(key : Any, db : DBType) : Future[Boolean]
}
